import cv2
import os
import h5py
import numpy as np
from tqdm import tqdm

MAX_SIZE = (256,256)

db = '/srv/new_train/FinalDirectory/'
im_files = []
with open('/home/rahul/multi-scale-rmac/image_paths.txt','r') as f:
    for line in f:
        im_files.append(db+line.rstrip())

surf = cv2.xfeatures2d.SURF_create(450)

hf = h5py.File('./SURF.hdf','a')
grp = hf.create_group('surf_vectors')


'''
for group by filename hdf
'''

for image in tqdm(im_files):
    subgrp = grp.create_group(image)
    data = subgrp.create_dataset('v', shape=(0,64),dtype=np.float32, maxshape=(None,64))
    cv_im = cv2.imread(image)
    #if cv_im.shape[0]>MAX_SIZE[0] or cv_im.shape[1]>MAX_SIZE[1]:
    cv_im = cv2.resize(cv_im, MAX_SIZE)
    kp, des = surf.detectAndCompute(cv_im, None)
    try:
        init_index = data.shape[0]
        data.resize((data.shape[0]+des.shape[0], 64))
        data[init_index:init_index+des.shape[0],:] = des
        hf.flush()
    except AttributeError as e:
        print('Image returns no SURF features..skipping')
        im_files.remove(image)
        continue

hf.close()
print('closing hf file.')

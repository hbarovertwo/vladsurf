import h5py
import pickle
import numpy as np
from numba import njit
from tqdm import tqdm

'''
Normalization scheme. Power transform: sign(z)*(z^0.5) for z in vector.
Followed by L2-normalization of vector.
'''
@njit
def Norm_scheme(vec):
    for elem in range(len(vec)):
        vec[elem] = np.sign(vec[elem])*np.sqrt(np.absolute(vec[elem]))
    vec = vec / np.sqrt(np.sum(vec**2))
    return vec

'''
VLAD aggregation algorithm for SURF descriptors of images.
->Load up SURF descriptors from HDF for each image if available.
->Load up the kmeans object from pickle file to get clusters.
->Loop over image filepaths, if they exist in HDF, store VLAD+SURF
into another HDF file.
'''
def VLAD(fpath, hdf_obj, kmeans):
    vladsurf = np.zeros((9600,))
    desc = hdf_obj['surf_vectors'][fpath]['v']
    for k in range(len(desc)):
        d = desc[k]
        cluster_id = kmeans.predict(d.reshape(1,64))[0]
        diff = d - kmeans.cluster_centers_[cluster_id]
        vladsurf[(cluster_id)*64:(cluster_id+1)*64] = vladsurf[(cluster_id)*64:(cluster_id+1)*64] + diff
        final = Norm_scheme(vladsurf)
    return final.astype('float32')

db = '/srv/new_train/FinalDirectory/'
im_files = []
with open('/home/rahul/multi-scale-rmac/image_paths.txt','r') as f:
    for line in f:
        im_files.append(db+line.rstrip())

# Load up Kmeans object stored in pickle format
with open('./kmeans/kmeans_22_150clusters.p','rb') as kmeans_object:
    kmeans_obj = pickle.load(kmeans_object)

# Load up SIFT HDF, indexed by filename
hdf = h5py.File('./SIFT.hdf','r')

# Create empty HDF to store VLAD+SURF vectors
hdf2 = h5py.File('./VLAD_SURF.hdf','a')
vecs = hdf2.require_group('vectors')
names = hdf2.require_group('filenames')
dset = vecs.create_dataset('v', shape=(0,9600),dtype=np.float32, maxshape=(None,9600))
dt = h5py.special_dtype(vlen=str)
nset = names.create_dataset('f', shape=(0,), maxshape=(None,), dtype=dt)

n = 0
for i in tqdm(range(len(im_files))):
    try:
        v = VLAD(im_files[i], hdf, kmeans_obj)
        init_index = dset.shape[0]
        dset.resize((dset.shape[0]+1,9600))
        dset[n,:] = v
        nset.resize((nset.shape[0]+1,))
        nset[n] = im_files[i]
        hdf2.flush()
        n += 1
    except (KeyError,UnboundLocalError):
        print('no surf features for this image, skipping')
        im_files.remove(im_files[i])
        continue

with open('indexed_ims.txt','w+') as f:
    for fpath in im_files:
        f.write(fpath+'\n')

hdf.close()
hdf2.close()

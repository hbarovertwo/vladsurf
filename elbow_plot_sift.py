import os
import h5py
import matplotlib.pyplot as plt
from sklearn.cluster import MiniBatchKMeans
from tqdm import tqdm
import pickle


hdf = h5py.File('./SIFT_cache.hdf','r')
dset = hdf['vectors']['v']

clustno = [64,72,97,105,115,125,150,175,200,225,250,275,300,325,350]

kmeans = [MiniBatchKMeans(n_clusters=i, batch_size=500) for i in clustno]

for i in range(len(clustno)):
    for r in tqdm(range(len(dset)//500)):
        kmeans[i].partial_fit(dset[r*500:(r+1)*500])
    pickle.dump(kmeans[i], open('kmeans_'+str(i)+'.p','wb'))
    print('pickled kmeans file object.')

score = [kmeans[k].inertia_ for k in range(len(kmeans))]

plt.plot(clustno, score)
plt.xlabel('Number of clusters')
plt.ylabel('K-Means intertia_')
plt.title('Elbow curve')
plt.savefig('elbow_plot.png')

print('saving plot')

hdf.close()

print('hdf closed successfully')



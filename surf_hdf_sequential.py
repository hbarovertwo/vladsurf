import cv2
import os
import h5py
import numpy as np
from tqdm import tqdm

MAX_SIZE = (1024,768)

db = '/srv/new_train/eu/3/'
im_files = []
with open('/home/rahul/multi-scale-rmac/image_paths.txt','r') as f:
    for line in f:
        im_files.append(db+line.rstrip())

surf = cv2.xfeatures2d.SURF_create(450)

'''
Uncomment names, dt, nset if you want to store 
filenames along with descriptors.
'''
hdf = h5py.File('./SURF_cache.hdf','a')
vecs = hdf.require_group('vectors')
#names = hdf.require_group('filenames')
dset = vecs.create_dataset('v', shape=(0,64),dtype=np.float32, maxshape=(None,64))
#dt = h5py.special_dtype(vlen=str)
#nset = names.create_dataset('f', shape=(0,), maxshape=(None,), dtype=dt)

ims_removed = 0
for image in tqdm(im_files):
    try:
        cv_im = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    #if cv_im.shape[0]>MAX_SIZE[0] or cv_im.shape[1]>MAX_SIZE[1]:
    #    cv_im = cv2.resize(cv_im, MAX_SIZE)
        kp, des = surf.detectAndCompute(cv_im, None)
        init_index = dset.shape[0]
        dset.resize((dset.shape[0]+des.shape[0], 64))
        dset[init_index:init_index+des.shape[0],:] = des
        #nset.resize((nset.shape[0]+des.shape[0],))
        #nset[init_index:init_index+des.shape[0]] = image
        hdf.flush()
    except Exception as e:
        print(image)
        im_files.remove(image)
        ims_removed += 1
        continue

hdf.close()

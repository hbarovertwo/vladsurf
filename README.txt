>Once both HDF's are done being formed, run elbow_plot.py
>Edit the range of cluster numbers if desired
>Can make the range in steps like 64 84 104 .. if desired.
>Analyze elbow plot.
>Load the corresponding pickle file of the best cluster number in vlad_to_hdf.py
>Move the HDF we no longer need for clustering to AWS.
>With the remaining HDF still in directory, run vlad_to_hdf.py. 
--------------------------------------------------------------
